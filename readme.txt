SUMMARY of the project
************************
This small project aims to find out all anagrams and palindromes inside an input file.
For details you can refer to the file at src\resources\Assignment_Developer.docx

Note: Palindrome concept is explained incorrectly in the project, still I went with it. 



DEPENDENCIES
************************
Gradle 2.0 :  for project automation
Java 1.7 



TESTS created:
************************
AnagramAndPalindromeStrategyTest: There are 3 tests here:
1) testStrategyReturnsEmptyArrayInCaseOfNoAnagrams
2) testStrategyReturnsTheOnlyAnagramGroup
3) testStrategyReturnsThePalindromeWithSpecialChar: this test is written for rare
special character (' for example) inside words. We should omit these characters during comparison.
Each of 3 test uses its own input file and asserts expected results.

AnagramTestGroupTest: Only 1 test here:
1) testAddToGroupActuallyAdds



INSTRUCTIONS
************************
Gradle should be available on your machine:

Gradle test: to do unit tests
Gradle jar: to create jar

run the jar with input and output file arguments to find any anagram and palindroms
For example to find anagrams and palindrome inside wordEn.txt and print results to output.ext; 
java -jar "Anagram and Palindrome.jar" wordsEn.txt output.txt



NOTES
************************
Currently there are two strategies:
1) Prime Numbers strategy: Tries to solve the problem by assigning prime numbers to each char. takes around 0,45 seconds to succeed.
2) Sorting strategy: Tries to solve the problem by sorting chars in the word. Takes around 0.21 seconds to succeed. Uses default sort strategy under Arrays Java class