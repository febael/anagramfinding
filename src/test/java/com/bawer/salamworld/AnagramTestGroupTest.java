package com.bawer.salamworld;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnagramTestGroupTest {

	@Test
	public void testAddToGroupActuallyAdds() {
		String s = "test";
		String s2 = "sett";
		AnagramTestGroup ag = new AnagramTestGroup((long) 1, s);
		ag.addToGroup("sett");
		assertEquals(1, ag.getGroupMembers().size());
		assertEquals(s2, ag.getGroupMembers().iterator().next());
	}

}
