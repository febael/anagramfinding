package com.bawer.salamworld;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class AnagramAndPalindromeStrategyTest {
	
	private AnagramAndPalindromeStrategy strategyImpl;

	public AnagramAndPalindromeStrategyTest(AnagramAndPalindromeStrategy strategyImpl) {
		this.strategyImpl = strategyImpl; 
	}

	@Test
	public void testStrategyReturnsEmptyArrayInCaseOfNoAnagrams() throws Exception {
		Path p = Paths.get(Thread.currentThread().getContextClassLoader().getResource("noAnagrams.txt").toURI());
		AnagramTestGroup[] ags = strategyImpl.execute(p, System.out);
		assertEquals(0, ags.length);
	}
	
	@Test
	public void testStrategyReturnsTheOnlyAnagramGroup() throws Exception {
		Path p = Paths.get(Thread.currentThread().getContextClassLoader().getResource("oneAnagramGroup.txt").toURI());
		AnagramTestGroup[] ags = strategyImpl.execute(p, System.out);
		assertTrue( CollectionUtils.isEqualCollection(ags[0].getGroupMembers(), 
				Arrays.asList(new String[] {"sert", "tres"})) );
	}
	
	@Test
	public void testStrategyReturnsThePalindromeWithSpecialChar() throws Exception {
		Path p = Paths.get(Thread.currentThread().getContextClassLoader().getResource(
				"twoAnagramGroupsOneWithSpecialCharAndOnePalindromeWithSpecialChar.txt").toURI());
		strategyImpl.execute(p, System.out);
		List<String> palindromicStrings = strategyImpl.getPalindromicStrings();
		assertEquals(1, palindromicStrings.size());
		assertTrue(palindromicStrings.get(0).equals("g'norw") || palindromicStrings.get(0).equals("wrong"));
	}

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][] { 
				{ new AnagramAndPalindromePrimeNumbersStrategy() } };
		return Arrays.asList(data);
	}
}
