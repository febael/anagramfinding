package com.bawer.salamworld;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class is created for testing the strategies created.
 * Note that, currently there is only one strategy to find anagrams and palindromes.
 * It contains only the main method which is the main class of the jar file it belongs to.
 * 
 * @author Bawer
 *
 */
public class AnagramAndPalindromeMain {

	/**
	 * 
	 * @param args	There are three cases depending on number of arguments provided:
	 * 				1) No args -> Use "wordsEn.txt file inside the jar as input, 
	 * and output to System.out.
	 * 				2) 1 arg -> Use the argument as the input file path, and output
	 * to System.out.
	 * 				3) 2 arg -> First argument is input file, and all output is written
	 * to the file given by second argument
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Path p;
		PrintStream ps;
		if (args.length < 1) {
			p = Paths.get(Thread.currentThread().getContextClassLoader().getResource("wordsEn.txt").toURI());
		} else {
			p = new File(args[0]).toPath();
		}
		if (args.length < 2) {
			ps = System.out;
		} else {
			ps = new PrintStream(new FileOutputStream(new File(args[1])));
		}
		AnagramTestGroup[] ags = new AnagramAndPalindromePrimeNumbersStrategy().execute(p, ps);
		ps.print("\n\n\n\n\n\n");
		ags = new AnagramAndPalindromeSortingStrategy().execute(p, ps);

	}

}
