package com.bawer.salamworld;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Bawer
 *
 */
public class AnagramAndPalindromeSortingStrategy implements AnagramAndPalindromeStrategy {

	private List<String> listOfPalindromicStrings = null;
	private long startTime;
	
	@Override
	public AnagramTestGroup[] execute(Path p, PrintStream ps) throws Exception {
		AnagramTestGroup[] ags = null;
		startTime = System.nanoTime();
		try {
			ags = createAnagramGroups(p);
			ags = filterAndPrintAnagrams(ps, ags);
			filterAndPrintPalindromes(ps, ags);
		} catch (Exception e) {
			throw e;
		}		
		return ags;	
	}
	
	private String[] filterAndPrintPalindromes(PrintStream ps, AnagramTestGroup[] ags) {
		if (ags.length == 0) {
			ps.printf("No Palindromes found");
			return new String[0];
		}
		this.listOfPalindromicStrings = new ArrayList<String>(ags.length/5);
		for (AnagramTestGroup ag : ags) {
			List<String> palindromicStrings = ag.getPalindromicStrings();
			if (palindromicStrings != null && palindromicStrings.size() > 0) {
				int tempIndex = 1;
				for (String s : palindromicStrings) {
 					ps.printf("\n%6d. %s", this.listOfPalindromicStrings.size() + tempIndex, s);
					tempIndex += 1;
				}
				this.listOfPalindromicStrings.addAll(palindromicStrings);
			}
		}
		if (this.listOfPalindromicStrings.size() == 0) {
			ps.printf("No Palindromes found");			
		} else {
			ps.printf("\n\n Statistics: No of palindromes: %d", this.listOfPalindromicStrings.size());
		}
		return this.listOfPalindromicStrings.toArray(new String[0]);
	}

	private AnagramTestGroup[] filterAndPrintAnagrams(PrintStream ps, AnagramTestGroup[] ags) {
		ps.printf("It took %d nanoseconds to find anagrams.\n\n", System.nanoTime() - startTime);
		if (ags.length == 0) {
			ps.printf("No Anagrams found");
			return ags;
		}
		List<AnagramTestGroup> listOfAnagramicGroups = new ArrayList<AnagramTestGroup>(ags.length/10);
		int noOfAnagramicWords= 0;
		for (AnagramTestGroup ag : ags) {
			if (ag.isAnagramic()) {
				listOfAnagramicGroups.add(ag);
				noOfAnagramicWords += 2;
				Iterator<String> groupMembers = ag.getGroupMembers().iterator();
				ps.printf("\n\n%6d. %-30s %s", listOfAnagramicGroups.size(), ag.getPrimeMember(),groupMembers.next());
				while (groupMembers.hasNext()) {
					noOfAnagramicWords += 1;
					ps.printf("\n%39s%s", "", groupMembers.next());
				}
			}
		}
		if (listOfAnagramicGroups.size() == 0) {
			ps.printf("No Anagrams found");			
		} else {
			ps.printf("\n\n Statistics: No of anagram groups: %d, no of anagram words: %d", listOfAnagramicGroups.size(), noOfAnagramicWords);
		}		
		return listOfAnagramicGroups.toArray(new AnagramTestGroup[0]);
	}

	private AnagramTestGroup[] createAnagramGroups(Path p) throws IOException {
		Map<String, AnagramTestGroup> mapOfAnagramGroups = new HashMap<String, AnagramTestGroup>();
		List<String> allWords = Files.readAllLines(p, Charset.forName("UTF-8"));
	    for (String word : allWords) {
	    	String sortedChars = getSortedCharsString(word);
	    	AnagramTestGroup anagramGroup = mapOfAnagramGroups.get(sortedChars);
	    	if (anagramGroup == null) {
	    		anagramGroup = new AnagramTestGroup(sortedChars, word);
	    		mapOfAnagramGroups.put(sortedChars, anagramGroup);
	    	} else {
	    		anagramGroup.addToGroup(word);
	    	}
	    	
	    }
	    return mapOfAnagramGroups.values().toArray(new AnagramTestGroup[0]);
	}

	private String getSortedCharsString(String w) {
		byte[] bytes = w.getBytes();
		Arrays.sort(bytes);
		int startIndex = 0;
		while (startIndex < bytes.length && bytes[startIndex] < 'a') {
			startIndex += 1;
		}
		int endIndex = bytes.length - 1;
		while (endIndex > startIndex && bytes[endIndex] > 'z') {
			endIndex -= 1;
		}
		return (endIndex-startIndex) == bytes.length-1 ? 
				new String(bytes) : new String(Arrays.copyOfRange(bytes, startIndex, endIndex));
	}

	@Override
	public List<String> getPalindromicStrings() {
		return this.listOfPalindromicStrings;
	}
}
