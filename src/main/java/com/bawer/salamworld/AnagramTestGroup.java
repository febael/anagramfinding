package com.bawer.salamworld;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class holds all candidates for an anagramic group.
 * Use {@link #isAnagramic() AnagramTestGroup} to actually check
 * if this is really an anagramic group.
 * 
 * @author Bawer
 *
 */
public class AnagramTestGroup {
	private List<String> members = null;
	private Object identifier;
	private String primeMember;
	
	public String getPrimeMember() {
		return primeMember;
	}

	public AnagramTestGroup(Object identifier, String primeMember) {
		this.identifier = identifier;
		this.primeMember = primeMember;
	}
	
	public void addToGroup(String s) {
		if (members == null) {
			members = new ArrayList<String>();
		}
		members.add(s);
	}
	
	public Set<String> getGroupMembers() {
		return members == null ? null : new HashSet<String>(members);
	}
	
	public boolean isAnagramic() {
		return members != null;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof AnagramTestGroup) {
			return this.identifier.equals(((AnagramTestGroup) o).identifier);
		}
		return false;
	}

	public List<String> getPalindromicStrings() {
		if (!isAnagramic()) {
			return null;
		}
		List<String> palindromicStrings = new ArrayList<String>();
		List<String> membersClone = new ArrayList<String>(members);
		int palindromeIndex = checkIfPalindromic(primeMember, members, 0);
		if (palindromeIndex != -1) {
			membersClone.remove(palindromeIndex);
			palindromicStrings.add(primeMember);
		}
		int i = 0;
		while (i < membersClone.size()-1) {
			palindromeIndex = checkIfPalindromic(membersClone.get(i), membersClone, i+1);
			if (palindromeIndex != -1) {
				palindromicStrings.add(membersClone.get(i));
				membersClone.remove(palindromeIndex);
			} else {
				i += 1;				
			}
		}
		return palindromicStrings;
	}

	private int checkIfPalindromic(String s, List<String> members, int memberIndex) {
		outerloop: 
		for (; memberIndex < members.size(); memberIndex++) {
			for (int i = s.length()-1, j = 0; i >= 0; i--, j++) { // Prevent a special character messing up
				if (s.charAt(i) < 'a' || s.charAt(i) > 'z') {
					j -= 1;
					continue;
				}
				if (members.get(memberIndex).charAt(j) < 'a' || members.get(memberIndex).charAt(j) > 'z') { // Prevent a special character messing up
					j += 1;
				}
				if (s.charAt(i) != members.get(memberIndex).charAt(j)) {
					continue outerloop;
				}
			}
			return memberIndex;
		}
		return -1;
	}
}