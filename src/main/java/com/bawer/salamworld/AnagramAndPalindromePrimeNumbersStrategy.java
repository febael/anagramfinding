package com.bawer.salamworld;

import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class uses PrimeNumbers strategy for finding anagrams.
 * Basically the approach depends on assigning first 26 prime numbers
 * to the letters in the English language according to letter frequency.
 * 
 * @author Bawer
 *
 */
public class AnagramAndPalindromePrimeNumbersStrategy implements AnagramAndPalindromeStrategy {

	private static final BigInteger[] ASCII_CHAR_TO_PRIME_NUMBER = new BigInteger['z'+1];
	static {
		final int[] first26PrimeNumbers = { 
				2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101 };
		Arrays.fill(ASCII_CHAR_TO_PRIME_NUMBER, BigInteger.ZERO);
		final char[] lettersSortedDescByFrequency = "etaoinshrdlcumwfgypbvkjxqz".toCharArray();
		int index = -1;
		for (char c : lettersSortedDescByFrequency) {
			ASCII_CHAR_TO_PRIME_NUMBER[c] = BigInteger.valueOf(first26PrimeNumbers[++index]);
		}
	}
	
	private List<String> listOfPalindromicStrings = null;
	private long startTime;
	
	public class UnexpectedCharException extends Exception { private static final long serialVersionUID = 1L; }

	@Override
	public AnagramTestGroup[] execute(Path p, PrintStream ps) throws Exception {
		AnagramTestGroup[] ags = null;
		startTime = System.nanoTime();
		try {
			ags = createAnagramGroups(p);
			ags = filterAndPrintAnagrams(ps, ags);
			filterAndPrintPalindromes(ps, ags);
		} catch (Exception e) {
			throw e;
		}		
		return ags;		
	}

	@Override
	public List<String> getPalindromicStrings() {
		return this.listOfPalindromicStrings;
	}
	
	private String[] filterAndPrintPalindromes(PrintStream ps, AnagramTestGroup[] ags) {
		if (ags.length == 0) {
			ps.printf("No Palindromes found");
			return new String[0];
		}
		this.listOfPalindromicStrings = new ArrayList<String>(ags.length/5);
		for (AnagramTestGroup ag : ags) {
			List<String> palindromicStrings = ag.getPalindromicStrings();
			if (palindromicStrings != null && palindromicStrings.size() > 0) {
				int tempIndex = 1;
				for (String s : palindromicStrings) {
 					ps.printf("\n%6d. %s", this.listOfPalindromicStrings.size() + tempIndex, s);
					tempIndex += 1;
				}
				this.listOfPalindromicStrings.addAll(palindromicStrings);
			}
		}
		if (this.listOfPalindromicStrings.size() == 0) {
			ps.printf("No Palindromes found");			
		} else {
			ps.printf("\n\n Statistics: No of palindromes: %d", this.listOfPalindromicStrings.size());
		}
		return this.listOfPalindromicStrings.toArray(new String[0]);
	}

	private AnagramTestGroup[] filterAndPrintAnagrams(PrintStream ps, AnagramTestGroup[] ags) {
		ps.printf("It took %d nanoseconds to find anagrams.\n\n", System.nanoTime() - startTime);
		if (ags.length == 0) {
			ps.printf("No Anagrams found");
			return ags;
		}
		List<AnagramTestGroup> listOfAnagramicGroups = new ArrayList<AnagramTestGroup>(ags.length/10);
		int noOfAnagramicWords= 0;
		for (AnagramTestGroup ag : ags) {
			if (ag.isAnagramic()) {
				listOfAnagramicGroups.add(ag);
				noOfAnagramicWords += 2;
				Iterator<String> groupMembers = ag.getGroupMembers().iterator();
				ps.printf("\n\n%6d. %-30s %s", listOfAnagramicGroups.size(), ag.getPrimeMember(),groupMembers.next());
				while (groupMembers.hasNext()) {
					noOfAnagramicWords += 1;
					ps.printf("\n%39s%s", "", groupMembers.next());
				}
			}
		}
		if (listOfAnagramicGroups.size() == 0) {
			ps.printf("No Anagrams found");			
		} else {
			ps.printf("\n\n Statistics: No of anagram groups: %d, no of anagram words: %d", listOfAnagramicGroups.size(), noOfAnagramicWords);
		}		
		return listOfAnagramicGroups.toArray(new AnagramTestGroup[0]);
	}

	private AnagramTestGroup[] createAnagramGroups(Path p) throws IOException, UnexpectedCharException {
		Map<BigInteger, AnagramTestGroup> mapOfAnagramGroups = new HashMap<BigInteger, AnagramTestGroup>();
		List<String> allWords = Files.readAllLines(p, Charset.forName("UTF-8"));
	    for (String word : allWords) {
	    	BigInteger hashCode = getSpecialHashCode(word);
	    	AnagramTestGroup anagramGroup = mapOfAnagramGroups.get(hashCode);
	    	if (anagramGroup == null) {
	    		anagramGroup = new AnagramTestGroup(hashCode, word);
	    		mapOfAnagramGroups.put(hashCode, anagramGroup);
	    	} else {
	    		anagramGroup.addToGroup(word);
	    	}
		}
	    return mapOfAnagramGroups.values().toArray(new AnagramTestGroup[0]);
	}

	private BigInteger getSpecialHashCode(String word) throws UnexpectedCharException {
		BigInteger hashCode = BigInteger.valueOf(1);
		for (byte b: word.getBytes()) {
			if (b > 'z' || b < ' ') { // ASCII-7
				throw new UnexpectedCharException();
			}
			if (!ASCII_CHAR_TO_PRIME_NUMBER[b].equals(BigInteger.ZERO)) {
				hashCode = hashCode.multiply(ASCII_CHAR_TO_PRIME_NUMBER[b]);
			}			
		}
		return hashCode;
	}
}
