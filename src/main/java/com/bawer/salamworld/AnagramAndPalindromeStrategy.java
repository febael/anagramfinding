package com.bawer.salamworld;

import java.io.PrintStream;
import java.nio.file.Path;
import java.util.List;

public interface AnagramAndPalindromeStrategy {
	/**
	 * This method is expected to give all the anagrams for an input {@link Path}
	 * 
	 * @param p		input file Path
	 * @param ps	output stream to write results and logs.
	 * @return		array of {@link AnagramTestGroup}
	 * @throws Exception
	 */
	AnagramTestGroup[] execute(Path p, PrintStream ps) throws Exception;
	
	/**
	 * This method is expected to return Palindromes after a successful call to 
	 * {@link #execute(Path, PrintStream) AnagramAndPalindromeStrategy}.
	 * 
	 * @return	{@link List} of palindromic strings. (only one of the two palindromic match.)
	 */
	List<String> getPalindromicStrings();
}
